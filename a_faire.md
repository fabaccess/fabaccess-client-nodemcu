
[ ] possibilite: comme on trouve dans les objets connectes on peut se connecter a un mini serveur web
sur l'objet pour sa configuration avec bouton sur son entree A0 pour la RAZ.

[x] ajouter bouton arret d'urgence (ex si plus de wifi, plus de serveur et urgence)
	a voir car il doit deja exister sur la machine

[x] synchroniser historique_autorisation et historique_badge

[x] si badge fixe:
lecture badge avec aide 
attente d'un badge admin pour mettre en marche
puis reposition badge aide pour laisser en marche
puis enleve badge pour arret 

[x] si badge mobile:
lecture badge avec aide 
attente d'un badge admin pour mettre en marche
puis lecture badge admin/aide pour arret 

[x] supprimer le car en bas a droite du lcd (info wifi)

[x] attention les messages avec accents
http://html.alldatasheet.com/html-pdf/63673/HITACHI/HD44780/4262/17/HD44780.html

[x] ok ajout petite tempo avant bip carte laisse en cas de demarrage

[x] ok ajout bip si on laisse le badge apres arret

[x] ok si erreur api i refaire la tentative de i

[x] ok verifier le watchdog

[x] ok afficher un message si en mode fixe on enleve la carte moins de 5s
    et faire clignoter la led

[x] ok changement de lecteur rfid

[x] ok on fait 2 lectures si identiques ok sinon recommence

[x] ok changer le fonctionnement de lecture_badge
ajout parametre phase 0 pas de badge 1 lecture en cours
2 un badge a ete lu

[x] ok afficher un message si en mode mobile la carte reste plus de 5s
    et faire clignoter la led

[x] ok pour arreter attendre mini 5s

[x] ok suppression des delay(3000) pour reaffichage votre badge svp

[x] ok parametrage mode affichage led pour relais eteind

[x] ok piloter le buzzer

[x] ok parametre le niveau du relais pour arret

[x] ok faire scroller les message trop long

[x] ok ajout du 0 pour l'autorisation (utilisation erronnee)

[x] ok changement 4 leds -> 1 led rgb

[x] ok verifier le fonctionnement des leds en marche reste eclairee
garde le message

[x] ok eteint apres 3s si arret remet votre badge svp

[x] ok en marche avec un manager badge mobile
    et la un user simple badge affiche rouge
    pas de droit message deja occupe
    mais au bout de 3s reafficher vert et manager

[x] ok gerer le cligno

