
#ifndef variable_globale_h
#define variable_globale_h

// **************************** Variables globales *****************************


struct _globale
{
	char idmachine[MACSIZE];																							// adresse mac du nodemcu

	char cligno=NOIR;																									// cligno = NOIR affichage led fixe sinon led cligno

	unsigned long scroll;																								// memorisation temps pour la gestion du scrolling
	char frequence_cligno;																								// gestion de la frequence de clignotement de la led

	String afficheur[LCD_LIGNE];																						// les x lignes qui doivent etre affichees
	char   inx[LCD_LIGNE];																								// index pour le scrolling des x lignes
	String memoire_afficheur[LCD_LIGNE];																				// memorisation des x lignes qui doivent etre affichees pour gerer les changements
	String memoire_string;																								// memorisation string afficher pendant changement du a un timer

	byte anti_slash[8] = {0,0,16,8,4,2,1,0};																			// creation du caractere 0 \ pour le lcd
	byte e_aigu[8] = {2,4,0,14,17,31,16,14};																			// creation du caractere 1 é pour le lcd
	byte e_grave[8] = {8,4,0,14,17,31,16,14};																			// creation du caractere 2 è pour le lcd
	byte e_chapeau[8] = {4,10,0,14,17,31,16,14};																		// creation du caractere 3 ê pour le lcd
	byte a_grave[8] = {8,4,0,14,1,15,17,15};																			// creation du caractere 4 à pour le lcd

	char type_module;																									// avec badge fixe f ou mobile m
	char courant_badge[RFIDREAD][RFIDSIZE];																				// les x lectures du badge courant
	char precedent_badge[RFIDSIZE];																						// lecture precedente
	char dernier_badge[RFIDSIZE];																						// dernier badge lu
	char action;																										// 1 marche 0 arret
	char lecture;																										// no lecture badge
	char nombre;																										// nombre de lecture sans badge
	
	char phase;																											// 0 pas de badge (pas eu le start)
																														// 1 lecture en cours (eu le start) 
																														// 2 badge lu (eu le fin)
																				
	
																														// 1 non autorise 2 aide 4 ok
	//unsigned long courant_temps;																						// temps ecoule depuis la derniere lecture
	//unsigned long memoire_temps;																						// temps ecoule depuis le dernier changement
	char index_badge;																									// index de courant_badge
	unsigned long appel_rfid;																							// pour gerer le temps entre deux appels a lecture_badge

	char frequence_buzzer;																								// pour frequence buzzer
	
	char table_tempo[NOMBRE_TIMER];																						// duree des 7 timers en secondes
	String memoire_bienvenue;																							// memorisation du message de bienvenue
	
	char badge[4][RFIDSIZE];																							// historique des badge [0] courant [1] pere [2] grand pere [3] arriere grand pere
	char autorisation[4];																								// historique des autorisations correspondent au badge
	
	unsigned long tmp_timer[NOMBRE_TIMER];																				// variable contenant l'heure pour gerer les 7 timers
	
	// evenements
	
	unsigned char evt_badge;																							// 01 mis un badge
																														// 02 enleve un badge
																				
	unsigned char evt_timer;																							// 01 timer 0 (badge manager svp) active attente fin
																														// 02 timer 1 (enlevez votre badge) active attente fin
																														// 04 timer 2 (non autorise) active attente fin
																														// 08 timer 3 (arret en cours) active attente fin
																														// 10 timer 4 (avant enlevez votre badge) active attente fin
																														// 20 timer 5 (badge mal insere) active attente fin
																														// 40 timer 6 (erreur serveur) active attente fin
	
	// sorties
	
	unsigned char etat_machine;																							// 01 machine en marche
	
																														// 02 buzzer actif
																				
																														// 04 led rgb rouge eclaire
																														// 08 led rgb vert eclaire
																														// 10 led rgb bleu eclaire
																														// 20 led rgb cligno
	
	//unsigned char lcd_message;																						// 01 (l) presentez votre badge
																														// 02 (s) badge manager svp (timer 0)
																														// 04 (l) enlevez votre badge (timer 1) 
																														// --> changement fonctionnement timer 1 plus utilise 
																														// --> garde message et buzzer jusqu'a enlevement du badge
																														// 08 (s) non autorise (non connecte, machine en panne ...) (timer 2)
																														// 10 (l) arret en cours (timer 3)
																														// 20 (s) bienvenue administrateur/concierge/user
																														// 40 (l) badge mal insere (timer 5)
																														// 80 (l) erreur serveur (timer 6)
} globale;

#endif		// variable_globale_h


