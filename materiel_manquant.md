
# Materiel manquant pour 3 prototypes

[ ] pour les led de puissance
	- 3 petits raditeurs
	- 3 alim 12v (10w maxi)
    version 50mA 2 BC547
	- 9 resistance de 12 ohm
	- 9 resistance de 2,2K ohm
    version 250mA 2 BC547 et 1 IRF740
	- 9 resistance de 2,2 ohm
    - 9 resistance de 100K ohm
	- 9 resistance de 2,2K ohm

[ ] pour les led rvb
	- 1 led rvb
	- 6 resistances de 220 ohm

[ ] 1 afficheur lcd

[ ] 3 buzzer actif

[x] 3 boitier pour le nodeMCU, le LCD et le lecteur de badge

[x] 3 boitier pour le contacteur, le petit relais, l'alim 5v et l'alim 12v
[ ]	6 prise femelle 2p 6A (pour le branchement des alim 5v et 12v)

[ ] cable de branchement (electrique)

[ ] fil souple (branchement electronique)

[ ] gaine thermo (isolation led)

[ ] 3 connecteur 4p (inutile souder directement)

[ ] 3 connecteur 2p (inutile souder directement)

[ ] 6 boullons diam 3 ou 4 mm (fixation circuit)



