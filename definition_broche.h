
#ifndef definition_broche_h
#define definition_broche_h

// ************************** Definition des broches ***************************

// rfid reader

#ifndef BR_RFIDSEND
#define BR_RFIDSEND		D7																								// lecteur RFID broche sout
#endif

#ifndef BR_RFIDRECEIVE
#define BR_RFIDRECEIVE	D4																								// lecteur RFID broche receive (non branche)
#endif

// led

#ifndef BR_LEDRGBR
#define BR_LEDRGBR		D3																								// led rouge (nodeMCU output only)
#endif

#ifndef BR_LEDRGBG
#define BR_LEDRGBG		D4																								// led verte (nodeMCU output only)
#endif

#ifndef BR_LEDRGBB
#define BR_LEDRGBB		D8																								// led bleue (nodeMCU output only)
#endif

// lcd

#ifndef BR_LCDSCL
#define BR_LCDSCL		D1																								// i2c scl
#endif

#ifndef BR_LCDSDA
#define BR_LCDSDA		D2																								// i2c sda
#endif

// relais

#ifndef BR_MACHINE
#define BR_MACHINE		D5																								// commande relais
#endif

// buzzer

#ifndef BR_BUZZER
#define BR_BUZZER		D6																								// commande d'un buzzer et/ou avertisseur lumineux
#endif

// broche libre

#ifndef BR_DSPARE
#define BR_DSPARE			D0																								// non utilisee
#endif

#ifndef BR_ASPARE
#define BR_ASPARE			A0																								// non utilisee
#endif

#endif		// definition_broche_h

