
/*
 * FabAccess.ino
 *
 *
 * Gestion de badge pour l'acces a un fablab
 * avec consommation d'une API
 *
 *
 * le materiel:
 * 		nodeMCU (esp8266+wifi)
 *		lecteur RFID
 *		afficheur LCD 2x16
 *		led RGB
 *		relais (adapter le #define ARRET en fonction du relais utilise)
 *
 *
 * librairies : 
 * - LiquidCrystal I2C de Frank de Brabander - Version 1.1.2
 * - ArduinoJson de BEnoit Blanchon - Version 5.13.5
 *
 *
 * toutes les broches sont utilisees sauf A0
 *
 *
 */

// ******************************** Librairies *********************************

#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <ArduinoJson.h>

// *************************** code wifi et URL api ****************************

#include "secret.h"

// *********************** parametres de configuration *************************

#include "parametre_configuration.h"

// ************************** Definition des broches ***************************

#include "definition_broche.h"

// *************************** Constantes materiels ****************************

#include "constante_materiel.h"

// ************************** Constantes du programme **************************

#include "constante_programme.h"

// ******************************** info debug *********************************

#include "debug.h"

// **************************** Variables globales *****************************

#include "variable_globale.h"

// ************************** Definition des objets ****************************

LiquidCrystal_I2C lcd(LCD_ADRESSE, LCD_COLONNE, LCD_LIGNE);																// adresse 0x27 ou 0x3f, 2 lignes de 16 caracteres
SoftwareSerial rfidSerial =  SoftwareSerial(BR_RFIDSEND, BR_RFIDRECEIVE);												// port serie logiciel pour lecteur RFID

// ************************* Declaration des fonctions *************************

void attente(char, unsigned int);
int requete_serveur(char, char *, char *, char *);
void couleur();
void couleur(char);
void affichage(String, char);
void lecture_badge(void);

// ************************ initialisation de la carte *************************

void setup()
{
	memset(globale.inx, 0, LCD_LIGNE);																					// initialisation des variables globales
	memset(&globale, 0, sizeof(_globale));
	globale.index_badge = 255;
    globale.frequence_buzzer = 0;
    globale.frequence_cligno = 0;
    
	// pin mode
	pinMode(BR_MACHINE,	OUTPUT);
	pinMode(BR_BUZZER,	OUTPUT);
    pinMode(BR_LEDRGBR,	OUTPUT);
    pinMode(BR_LEDRGBG,	OUTPUT);
    pinMode(BR_LEDRGBB,	OUTPUT);
    pinMode(BR_RFIDSEND,INPUT);
    pinMode(BR_DSPARE,	INPUT);
    pinMode(BR_ASPARE,	INPUT);

    globale.etat_machine = 0;																							// machine arret buzzer arret led eteinte
    couleur();																											// eteint la led
    
    // loop() pas encore active doit etre fait a la main (sinon equivalent a la ligne du dessus)
    
    digitalWrite(BR_MACHINE, ARRET);																					// coupe l'alim de la machine
    digitalWrite(BR_BUZZER, LOW);																						// coupe le buzzer

	globale.table_tempo[0] = TEMPSMANAGER / 1000;
	globale.table_tempo[1] = TEMPSENLEVE / 1000;
	globale.table_tempo[2] = TEMPSNOVICE / 1000;
	globale.table_tempo[3] = TEMPSARRET / 1000;
	globale.table_tempo[4] = TEMPSATTENTE / 1000;
	globale.table_tempo[5] = TEMPSPOSITION / 1000;
	globale.table_tempo[6] = TEMPSERREUR / 1000;

    // moniteur
#ifdef DEBUG
    Serial.begin(115200);
    while (!Serial);				          																			// attend qu'il soit pret
    Serial.println("\n\tClient web");
#endif

    // lecteur RFID
    rfidSerial.begin(RFIDBAUD);			           																		// vitesse port serie logiciel
#ifdef DEBUG
    Serial.println("Lecteur RFID initialisé");
#endif

    // afficheur LCD
    lcd.init();
    lcd.backlight();
    lcd.clear();
#ifdef DEBUG
    Serial.println("Afficheur LCD initialisé");
#endif

	// creation des caracteres inconnus du lcd (si on a la rom asiatique et non la rom europe)
	
	lcd.createChar(0, globale.anti_slash);
	lcd.createChar(1, globale.e_aigu);
	lcd.createChar(2, globale.e_grave);
	lcd.createChar(3, globale.e_chapeau);
	lcd.createChar(4, globale.a_grave);

    // connection WIFI
    
    couleur(BLEU);																										// couleur bleu en attendant le wifi
    
#ifdef DEBUG
	Serial.println("Connection au wifi ...");
#endif
	lcd.setCursor(0, 0);
    lcd.print(MESSAGE_WIFI);
	lcd.setCursor(0, 1);
    lcd.print("Connection... ");

    WiFi.begin(SSID, PSSW);
	attente(WIFI, 0);

    // trace
#ifdef DEBUG
    Serial.print("Wifi connecté en ");
    Serial.println(WiFi.localIP());
#endif

	// affiche le message attente serveur	    
	lcd.setCursor(0, 0);
    lcd.print(MESSAGE_ACCUEIL);

	// lecture mac adresse du module
	byte mac[6];
	WiFi.macAddress(mac);
	sprintf(globale.idmachine, "%02x:%02x:%02x:%02x:%02x:%02x", 
		mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);

	// demande au serveur les infos pour ce module
	while (requete_serveur('i', globale.idmachine, NULL, NULL) == -1)
		attente(MILLISECONDE, 60000);
    
	// afficheur
	
	affichage(String(MESSAGE_BADGE), 1);

	globale.scroll = millis();																							// memorisation temps actuel pour scrolling
}

// affichage caractere tournant
// parametre pc_wifi != 0 attente wifi sinon pi_attente
// parametre pi_attente != 0 => duree d'attente

void attente(char pc_wifi, unsigned int pi_attente)
{
    int i = 0;
	char aff[5] = "|/-.";
	
    while (1)
    {
		if (pc_wifi)
		{
			if (WiFi.status() == WL_CONNECTED)
			{
				couleur();
				break;
			}
		}
		else
		{
			 if (pi_attente < 300)
				break;
		    pi_attente -= 300;
		}
				
		lcd.setCursor(15, 1);
	    if (i == 3)
	    	lcd.write(0);
	    else
	    	lcd.print(aff[i]);
	    i++;
	    if (i > 3)
	        i = 0;
	        
	    delay(300);
    }

	lcd.setCursor(15, 1);
   	lcd.print(" ");
}

/*
	entree
		pc_demande i init d debut f fin a autorisation
		pp_mac adresse mac du node mcu
		pp_badge no badge lu par lecteur rfid (null si pc_demande==i)
		pp_admin no badge de l'admin pour autoriser (null autre cas)
	sortie
		positionne les leds d'autorisation
		affiche le resultat sur le lcd et console
	retour
		-1 en cas d'erreur
		autorisation
*/

int requete_serveur(char pc_demande, char *pp_mac, char *pp_badge, char *pp_admin)
{
    String nom="";																										// nom du module
    String type="";																										// type insertion du badge
    int	   autorisation=0;																								// autorisation de l'utilisateur
    String message="";																									// message affiche sur la 2e ligne

	HTTPClient http;
    String json;
    int httpCode;
	//char vide[LCD_COLONNE+1];
	
	//memset(vide, ' ', LCD_COLONNE);
	//vide[LCD_COLONNE] = 0;

	couleur(BLEU);																										// couleur bleu pendant la com avec le serveur

	// initialise l'objet qui gere le protocole HTTP
	if (pc_demande == 'i')
		http.begin(String(API) + "init");
	else if (pc_demande == 'd')
		http.begin(String(API) + "debut");
	else if (pc_demande == 'f')
		http.begin(String(API) + "fin");
	else if (pc_demande == 'a')
		http.begin(String(API) + "aide");
	else if (pc_demande == 'v')
		http.begin(String(API) + "veriffin");

	http.addHeader("Content-Type", "application/json");
	if (pp_badge == NULL)	
	{
		json = String("{\"idmachine\":\"") +
					pp_mac + "\"}";
	}
	else if (pp_admin == NULL)
	{
		json = String("{\"idmachine\":\"") +
					pp_mac + "\",\"idbadge\":\"" +
					pp_badge + "\"}";
	}
	else
	{
		json = String("{\"idmachine\":\"") +
					pp_mac + "\",\"idbadge\":\"" +
					pp_badge + "\",\"idadmin\":\"" +
					pp_admin + "\"}";
	}

#ifdef DEBUG
	Serial.print("==> ");
	Serial.print(pc_demande);
	Serial.print(" json=");
	Serial.println(json);
#endif

	httpCode = http.POST(json);        																					// envoi de notre json

	// test code http
	if (httpCode == 200)		            																			// reponse 200 = ok
	{
		String reponse = http.getString();
		
#ifdef DEBUG
		Serial.print("<== json=");
		Serial.println(reponse);
#endif

		// convertion Json en objet
		DynamicJsonBuffer jsonBuffer;
		JsonObject &repjson = jsonBuffer.parseObject(reponse);

		// controle la convertion du json
		if (repjson.success())	 	// ok
		{
			// pour api init
		    nom = repjson["nom"].as<String>();
		    type = repjson["type"].as<String>();

			// pour api debut ou fin
		    autorisation = repjson[String("autorisation")];
		    message = repjson["message"].as<String>();
		}
		else				            																				// erreur convertion json
		{
		
#ifdef DEBUG
		    Serial.println("erreur convertion json");
#endif

			globale.memoire_string = globale.afficheur[1];
			affichage(String(MESSAGE_ERREUR), 1);
			globale.evt_timer |= (1 << TIMER_ERREUR);
			globale.tmp_timer[TIMER_ERREUR] = millis();
			
			http.end();								 																	// fin com http
			
			couleur(BLEU | CLIGNO);																						// erreur avec le serveur bleu cligno
		    return(-1);
		}
	}
	else							    																				// reponse != 200
	{
	
#ifdef DEBUG
		Serial.print("<-- http code=");
		Serial.println(httpCode);
#endif
		
		globale.memoire_string = globale.afficheur[1];
		affichage(String(MESSAGE_ERREUR), 1);
		globale.evt_timer |= (1 << TIMER_ERREUR);
		globale.tmp_timer[TIMER_ERREUR] = millis();
		
		http.end();								 																		// fin com http
		
		couleur(BLEU | CLIGNO);																							// erreur avec le serveur bleu cligno
		return(-1);
	}
	http.end();								 																			// fin com http

	// passe ici que si il n'y a pas eu d'erreur avec le serveur
	
	if (pc_demande == 'i')																								// c'etait une requete d'initialisation
	{
		globale.etat_machine = 0;																						// initialisation complete de la machine
		
		affichage(nom, 0);																								// affichage nom machine 1ere ligne

		if (type == "mobile")
			globale.type_module = 'm';
		else if (type == "fixe")
			globale.type_module = 'f';
		else
			globale.type_module = ' ';
			
		couleur();
		return(4);
	}
	else if (pc_demande == 'v')
	{
		affichage(message, 1);
		couleur();
		return(autorisation);
	}
	else
	{
		globale.memoire_string = globale.afficheur[1];
		affichage(message, 1);

		// eclaire les leds en fonction des droits de l'utilisateur
		if (autorisation == 4)																							// autorise
		{
			globale.etat_machine &= ~BUZZER;																			// arret buzzer
		  
		  	// trace
#ifdef DEBUG
		  	Serial.print("relais: ");
#endif

		  	if ((globale.etat_machine & MACHINE) == MACHINE_ARRETEE)													// la machine est l'arret on la demarre
		  	{
			  	globale.action = 1;																						// memorise action demarrage machine
			  	
			  	globale.etat_machine |= MACHINE_EN_MARCHE;																// positionne l'etat de la machine sur marche
			  	
			  	globale.etat_machine |= VERT;																			// positionne l'etat de la machine sur vert
			  	globale.etat_machine &= ~(ROUGE | BLEU | CLIGNO);														// depositionne cligno, rouge et bleu

			  	globale.memoire_bienvenue = message;																	// memorisation du message de bienvenue
			  	
			  	if (globale.type_module == 'm')																			// mode mobile
			  	{
					globale.evt_timer |= (1 << TIMER_ATTENTE);															// lancement timer attente avant message enlevez votre badge
					globale.tmp_timer[TIMER_ATTENTE] = millis();
			  	}
			  	else if (pc_demande == 'a')																				// mode fixe il y a eu une demande de validation par le fabmanager
			  	{
					globale.evt_timer |= (1 << TIMER_MANAGER);															// le manager vient de passer son badge reactive le timer manager pour autoriser l'echange de badge
					globale.tmp_timer[TIMER_MANAGER] = millis();
			  	}

#ifdef DEBUG
		  	  	Serial.println("0 --> 1");
#endif

		  	}
		  	else																										// la machine etait en marche
		  	{
		      	globale.action = 0;																						// memorise action arret machine
		    
			  	globale.etat_machine = MACHINE_ARRETEE;																	// arret de la machine du buzzer et de la led
			
#ifdef DEBUG
		  		Serial.println("1 --> 0");
#endif

		  	}
		}
		else if (autorisation == 2)																						// avec manager
		{
			globale.action = 2;																							// memorise action avec manager
			
			globale.etat_machine |= (ROUGE | VERT | CLIGNO);															// orange cligno
			globale.etat_machine &= ~(BLEU);
			
			globale.evt_timer |= (1 << TIMER_MANAGER);																	// utilisateur avec aide
			globale.tmp_timer[TIMER_MANAGER] = millis();
		}
		else if (autorisation == 1)																						// non autorise
		{
			globale.action = 4;																							// memorise action non autorise
			
			globale.etat_machine |= ROUGE;																				// rouge
			globale.etat_machine &= ~(VERT | BLEU | CLIGNO);
			
			globale.evt_timer |= (1 << TIMER_NOVICE);
			globale.tmp_timer[TIMER_NOVICE] = millis();
		}
		else if (autorisation == 8)																							// en panne
		{
			globale.etat_machine &= ~BUZZER;																			// arret buzzer
		  
		  	globale.action = 8;																							// memorise action demarrage machine
			  	
		  	globale.memoire_bienvenue = message;																		// memorisation du message de bienvenue
			  	
		  	if (globale.type_module == 'm')																				// mode mobile
		  	{
				globale.evt_timer |= (1 << TIMER_ATTENTE);																// lancement timer attente avant message enlevez votre badge
				globale.tmp_timer[TIMER_ATTENTE] = millis();
		  	}
		}
		else																											// erreur d'utilisation (ex badge sur machine active)
		{
			globale.action = 16;																						// memorise action erreur
			
			couleur(ROUGE | CLIGNO);																					// rouge cligno

			globale.etat_machine |= BUZZER;																				// commande du buzzer
		}
	}

	couleur();
	return (autorisation);
}

// sans parametre l'affichage reel de l'etat de la machine
// avec parametre affichage d'une couleur provisoire (setup ou acces serveur)

// parametre couleur
// si NOIR (=0) eteint la led rgb
// si cligno faire clignoter la led dans loop()
// si ledvert ledorange ou ledrouge on eclaire la led la ou les couleurs

void couleur(void)
{
	if ((globale.frequence_cligno & (ROUGE|VERT|BLEU|CLIGNO)) == (globale.etat_machine & (ROUGE|VERT|BLEU|CLIGNO)))		// la couleur de la led n'a pas change
		return;
		
	if (globale.etat_machine & ROUGE)																					// change la couleur
		digitalWrite(BR_LEDRGBR, HIGH);
	else
		digitalWrite(BR_LEDRGBR, LOW);
		
	if (globale.etat_machine & VERT)	
		digitalWrite(BR_LEDRGBG, HIGH);
	else
		digitalWrite(BR_LEDRGBG, LOW);
		
	if (globale.etat_machine & BLEU)	
		digitalWrite(BR_LEDRGBB, HIGH);
	else
		digitalWrite(BR_LEDRGBB, LOW);
		
	globale.frequence_cligno = globale.etat_machine & (ROUGE | VERT | BLEU | CLIGNO);									// memorise la nouvelle couleur
}

void couleur(char pc_couleur)
{
	globale.frequence_cligno = ROUGE | VERT | BLEU;
	
	if (pc_couleur & ROUGE)																								// change la couleur
		digitalWrite(BR_LEDRGBR, HIGH);
	else
		digitalWrite(BR_LEDRGBR, LOW);
		
	if (pc_couleur & VERT)	
		digitalWrite(BR_LEDRGBG, HIGH);
	else
		digitalWrite(BR_LEDRGBG, LOW);
		
	if (pc_couleur & BLEU)	
		digitalWrite(BR_LEDRGBB, HIGH);
	else
		digitalWrite(BR_LEDRGBB, LOW);
}

//
// affichage d'un string sur le lcd
//
// par1 = message
// par2 = no ligne (1ere = 0)

void affichage(String ps_message, char pc_ligne)
{
	//globale.inx[pc_ligne] = 0;
				
	// correction des accents
	
	char e_aigu[]	= {1, 0};																							// creation d'une chaine "é" pour string.replace()
	char e_grave[]	= {2, 0};																							// creation d'une chaine "è" pour string.replace()
	char e_chapeau[]= {3, 0};																							// creation d'une chaine "ê" pour string.replace()
	char a_grave[]	= {4, 0};																							// creation d'une chaine "à" pour string.replace()
	
	ps_message.replace("u00e9", e_aigu);
	ps_message.replace("u00e8", e_grave);
	ps_message.replace("u00ea", e_chapeau);
	ps_message.replace("u00e0", a_grave);
	
	if (ps_message.length() > LCD_COLONNE)
		globale.afficheur[pc_ligne] = ps_message + "    " + ps_message.substring(0, LCD_COLONNE);						// memorisation du message + affichage dans loop()
	else
		globale.afficheur[pc_ligne] = ps_message;
}

//
// acces au lecteur de badge et traitement correspondant
//

void lecture_badge(void)
{
	unsigned long ll_delta;

    // Serial RFID Reader
    /*  When the RFID Reader is active and a valid RFID tag is placed 
    	with range of the reader, the tag's unique ID will be transmitted 
    	as a 12-byte printable ASCII string (ID) to the host 
    	(start byte + ID + stop byte)
        For example, for a tag with a valid ID of 0F0184F07A00, 
        the following bytes would be sent: 0x02, 0x30, 0x46, 0x30, 0x31, 0x38,
        0x34, 0x46, 0x30, 0x37, 0x41, 0x30, 0x30, 0x03 We'll receive the ID and
        convert it to a null-terminated string with no start or stop byte.
    */
	
	while (rfidSerial.available() > 0)      																			// des caracteres sont dispo dans le lecteur
	{
		if (globale.phase == 0)																							// attente du caractere de debut de badge
		{
			globale.courant_badge[globale.lecture][0] = rfidSerial.read();												// lecture
			if (globale.courant_badge[globale.lecture][0] != RFIDSTART)													// si pas le car de debut continue d'attendre
			{
				delay(1);
				continue;
			}
			globale.index_badge = 0;																					// sinon on a lu le start
			globale.nombre = 0;																							// compteur vide
			globale.phase++;																							// init de l'index et changement de phase
		}
		else if (globale.phase == 1)																					// attente du car de fin
		{
			globale.courant_badge[globale.lecture][globale.index_badge] = rfidSerial.read();							// lecture du car
			if (globale.courant_badge[globale.lecture][globale.index_badge] == RFIDSTOP)								// si car de fin
			{
				if (globale.index_badge == (RFIDSIZE-1))																// a la fin -> ok
				{
					globale.courant_badge[globale.lecture][RFIDUTIL] = 0;												// efface car fin et transformation en chaine
					globale.phase++;
					break;																								// fin lecture
				}
				else																									// car de fin au mauvais endroit (efface tout)
					globale.phase = 0;
			}
			
			globale.index_badge++;
			if (globale.index_badge >= RFIDSIZE)																		// debordement pas trouve la fin
				globale.phase = 0;
			
			delay(1);
		}
		else
			globale.phase = 0;
	}
	//rfidSerial.flush();																								// lu ou non un badge efface le reste

	if (globale.phase == 1)																								// lecture en cours
		return;
		
	if (globale.phase == 0)																								// pas de badge
	{
		globale.nombre++;																								// x lectures vide avant de dire pas de badge
		if (globale.nombre < RFIDEMPTY)
			return;
		globale.nombre = 0;
		globale.courant_badge[globale.lecture][0] = 0;																	// badge vide
	}
	
	// lecture complete (vide ou badge)
	globale.lecture++;																									// gestion des lectures completes (veux x)
	if (globale.lecture < RFIDREAD)
	{
		globale.phase = 0;
		return;
	}

	globale.lecture = 0;	
	for (char n=0; n<RFIDREAD-1; n++)
	{
		if (strcmp(globale.courant_badge[n], globale.courant_badge[n+1]))												// compare les 2 valeurs lues
		{
			globale.phase = 0;
//Serial.println("erreur de lecture");																					// temps entre 2 erreurs donne une indication sur la bonne position du badge
			return;																										// si differente on fait rien (relecture de x autres)
		}
	}
	
	// les x valeurs lues sont identiques (considere comme bonne)
	// copie dans globale.badge[COURANT]
	strcpy(globale.badge[COURANT], globale.courant_badge[0]);
	
	if (globale.phase == 0)																								// pas de badge
	{
		if (globale.badge[PERE][0] != 0)																				// pas de badge ... il y en avait un --> badge enleve
		{
			globale.etat_machine &= ~BUZZER;																			// arret buzzer

			if ((globale.etat_machine & MACHINE) == MACHINE_ARRETEE)													// machine arrete
			{
				if (globale.type_module == 'm')																			// machine arrete et mobile
				{
					globale.evt_timer &= ~(1 << TIMER_ATTENTE);															// arret du timer attente

					globale.etat_machine &= ~(ROUGE | VERT | BLEU | CLIGNO);											// eteint la led
					
					if (!(globale.evt_timer & (1 << TIMER_MANAGER)))
						if (globale.action == 8)																		// en panne
							affichage(globale.memoire_bienvenue, 1);													// reaffichage du message en panne
						else
							affichage(String(MESSAGE_BADGE), 1);														// message presentez votre badge
				}
			}
			else																										// machine en marche
			{
				if (globale.type_module == 'm')																			// machine en marche et mobile
				{
					globale.evt_timer &= ~(1 << TIMER_ARRET);															// arret du timer arret
					globale.evt_timer &= ~(1 << TIMER_ATTENTE);															// arret du timer attente
					
					globale.etat_machine |= VERT;																		// eclaire la led en vert
					globale.etat_machine &= ~(CLIGNO | ROUGE | BLEU);
					
					affichage(globale.memoire_bienvenue, 1);															// reaffichage du message de bienvenue
				}
				else																									// machine en marche et fixe
				{
					globale.etat_machine |= CLIGNO;																		// fait clignoter la led
					
					globale.memoire_string = globale.afficheur[1];														// lancement timer arret
					affichage(String(MESSAGE_ARRET), 1);
					globale.evt_timer |= (1 << TIMER_ARRET);
					globale.tmp_timer[TIMER_ARRET] = millis();
				}
			}

			globale.badge[PERE][0] = 0;
		}
		else
		{
			if ((globale.etat_machine & MACHINE) == MACHINE_ARRETEE)
				if (globale.type_module == 'm')
					if (!(globale.evt_timer & (1 << TIMER_MANAGER)))
						if (globale.action == 8)
							affichage(globale.memoire_bienvenue, 1);													// reaffichage du message de bienvenue
						else
							affichage(String(MESSAGE_BADGE), 1);														// message presentez votre badge
		}
	}
	else																												// un badge a ete lu
	{
		if (globale.badge[PERE][0] == 0)																				// un badge a ete lu ... nouvelle lecture
		{
			if ((globale.etat_machine & MACHINE) == MACHINE_ARRETEE)													// machine a l'arret
			{
				globale.etat_machine &= ~(BUZZER | CLIGNO);																// arrete le buzzer et le cligno
				
				if (globale.evt_timer & (1 << TIMER_MANAGER))															// timer pas encore ecoule
					globale.autorisation[COURANT] = 
							requete_serveur('a', globale.idmachine, globale.badge[GRAND], globale.badge[COURANT]);		// mise en marche si autorise
				else
					globale.autorisation[COURANT] = 
							requete_serveur('d', globale.idmachine, globale.badge[COURANT], NULL);						// mise en marche si autorise
			}
			else																										// machine en marche
			{
				globale.action = 0;
				if (globale.type_module == 'f')																			// en mode fixe 
				{
					if ((globale.evt_timer & (1 << TIMER_MANAGER)) ||													// temps pour changer de badge
						(!strcmp(globale.badge[COURANT], globale.badge[GRAND])))										// de nouveau le badge avec aide
					{
						globale.evt_timer &= ~(1 << TIMER_ARRET);														// arret timer d'arret

						globale.etat_machine &= ~(BUZZER | CLIGNO);														// arrete le cligno et le buzzer
						affichage(globale.memoire_bienvenue, 1);
					}
				}
				else
				{
					if ((globale.autorisation[COURANT] = requete_serveur('v', globale.idmachine, globale.badge[COURANT], NULL)) != 4)	// controle les droits pour resoudre le probleme affichage arret et en fin non pas autorise
					{
						globale.etat_machine |= BUZZER;																	// demarre le buzzer
						couleur(ROUGE | CLIGNO);																		// rouge cligno cligno
					}
					else
					{
						if (globale.type_module == 'm')																	// mode mobile
						{
							globale.etat_machine |= CLIGNO;																// cligno led
							
							globale.memoire_string = globale.afficheur[1];												// lancement timer arret
							affichage(String(MESSAGE_ARRET), 1);
							globale.evt_timer |= (1 << TIMER_ARRET);
							globale.tmp_timer[TIMER_ARRET] = millis();
						}
					}
				}
			}
			
			strcpy(globale.badge[PERE], globale.badge[COURANT]);														// il n'y avait pas de pere
			globale.autorisation[PERE] = globale.autorisation[COURANT];
		}
		else																											// un badge a ete lu ... reste dans la meme lecture
		{
			if (strcmp(globale.badge[PERE], globale.badge[COURANT]))													// relecture mais d'un badge different (possible ?)
			{
				if ((globale.type_module == 'f') && ((globale.etat_machine & MACHINE) != MACHINE_ARRETEE))				// mode fixe et machine en marche
				{
					globale.etat_machine |= CLIGNO;																		// cligno led
					
					globale.etat_machine |= BUZZER;																		// demarre le buzzer
					globale.memoire_string = globale.afficheur[1];														// lancement timer arret
					affichage(String(MESSAGE_ARRET), 1);
					globale.evt_timer |= (1 << TIMER_ARRET);
					globale.tmp_timer[TIMER_ARRET] = millis();
				}
			}
			else																										// plusieur lecture de suite meme badge (pere == courant)
			{
				if (strcmp(globale.badge[GRAND], globale.badge[COURANT]))												// grand != courant
				{
					strcpy(globale.badge[ARRIERE], globale.badge[GRAND]);														
					globale.autorisation[ARRIERE] = globale.autorisation[GRAND];
					
					strcpy(globale.badge[GRAND], globale.badge[PERE]);
					globale.autorisation[GRAND] = globale.autorisation[PERE];
				}
			}
		}
		
		globale.phase = 0;
	}
}

// ***************************** boucle principale *****************************

void loop()
{	
	char ligne, longueur;
	char vide[LCD_COLONNE+1];
	
	memset(vide, ' ', LCD_COLONNE);
	vide[LCD_COLONNE] = 0;

	// pilotage machine
	
	if (globale.etat_machine & MACHINE)
		digitalWrite(BR_MACHINE, MARCHE);
	else
		digitalWrite(BR_MACHINE, ARRET);

	// pilotage buzzer
		
	if (globale.etat_machine & BUZZER)
	{
		globale.frequence_buzzer = 1 - globale.frequence_buzzer;
		if (globale.frequence_buzzer)
			digitalWrite(BR_BUZZER, HIGH);
		else
			digitalWrite(BR_BUZZER, LOW);
	}
	else
		digitalWrite(BR_BUZZER, LOW);

	// pilotage led
	
	if (globale.etat_machine & CLIGNO)
	{	
		if (millis() & VITESSE)
		{
			if (!(globale.frequence_cligno & 1))
			{
				globale.frequence_cligno |= 1;
				couleur();
			}
		}
		else
		{
			if (globale.frequence_cligno & 1)
			{
				globale.frequence_cligno &= ~1;
				couleur(NOIR);
			}
		}
	}
	else
		couleur();																										// remet la couleur on a pu s'arrete sur eteint pendant le cligno

	// verification du wifi
	
    if (WiFi.status() != WL_CONNECTED)        																			// plus de connection  au wifi
    {
		couleur(BLEU|CLIGNO);

    	attente(WIFI, 0);
	}

	// lecture badge
	
 	if ((millis() - globale.appel_rfid) > RFIDCALL)
 	{
 		globale.appel_rfid = millis();
		lecture_badge();
	}

	// gestion des timers
	
	for (int x=0,b=1; x<NOMBRE_TIMER; x++,b<<=1)																		// pour les x timers
	{
		if (globale.evt_timer & b)																						// le timer est actif
		{
			if ((millis() - globale.tmp_timer[x]) > (globale.table_tempo[x] * 1000))									// timer expere
			{
				if (x == TIMER_ARRET)																					// fin du timer arret
				{
					if ((globale.etat_machine & MACHINE) != MACHINE_ARRETEE)											// machine en marche
					{
						if (globale.type_module == 'm')
						{
							globale.evt_timer |= (1 << TIMER_ATTENTE);													// lancement timer attente avant message enlevez votre badge
							globale.tmp_timer[TIMER_ATTENTE] = millis();
						}

						if (globale.autorisation[GRAND] < 4)
							globale.autorisation[0] = requete_serveur('f', globale.idmachine, globale.badge[ARRIERE], NULL);	// arret
						else
							globale.autorisation[0] = requete_serveur('f', globale.idmachine, globale.badge[GRAND], NULL);	// arret
					}
				}

				if (x == TIMER_ATTENTE)																					// fin du timer attente
				{
					globale.etat_machine |= BUZZER;																		// demarrage du buzzer
					
					couleur(ROUGE | CLIGNO);
					
					affichage(String(MESSAGE_ENLEVE), 1);
				}

				if (x == TIMER_MANAGER)																					// fin du timer manager
				{
					affichage(String(MESSAGE_BADGE), 1);																// message presentez votre badge
				}

				globale.evt_timer &= ~b;																				// desactive le timer
			}
			else if (x == TIMER_ARRET)
			{
				if (globale.afficheur[1].substring(0, String(MESSAGE_ARRET).length()) == String(MESSAGE_ARRET))			// c'est le message d'arret (attention scrolling interdit doit etre court)
				{
					String tmp = String(MESSAGE_ARRET) + 
									" " + 
									(globale.table_tempo[x] - ((millis() - globale.tmp_timer[x]) / 1000)) + 
									"s";																				// genere le texte

					if (globale.afficheur[1] != tmp)																	// les secondes ont changees
						globale.afficheur[1] = tmp;																		// texte court direct
				}
			}
		}
	}
	
	//	gestion de l'affichage sans blocage de la loop
	//		rafraichissement affichage si il a change
	//		avec scrolling des lignes trop longues
	//		effacement des car de droite pour lignes plus courtes
	//		remplacement des accents

	if ((millis() - globale.scroll) > SCROLLING)																		// temps ecoule pour le scolling
	{
		globale.scroll = millis();																						// reinitialisation temps scrolling
		
		for (ligne=0; ligne<LCD_LIGNE; ligne++)																			// toute les lignes du lcd
		{
			longueur = globale.afficheur[ligne].length();																// longueur de la ligne courante a afficher
			lcd.setCursor(0, ligne);																					// positionnement curseur
			if (longueur > (LCD_COLONNE))																				// texte ne rentre pas dans une ligne -> scrolling + changement texte
			{
				globale.inx[ligne]++;
				if (globale.inx[ligne] >= (longueur - LCD_COLONNE))
					globale.inx[ligne] = 0;
				globale.memoire_afficheur[ligne] = globale.afficheur[ligne].substring(globale.inx[ligne], LCD_COLONNE+globale.inx[ligne]);
				lcd.print(globale.memoire_afficheur[ligne]);
			}
			else																										// il n'y a pas de scrolling controle si le texte a change
			{
				if (globale.afficheur[ligne] != globale.memoire_afficheur[ligne])
				{
					globale.memoire_afficheur[ligne] = globale.afficheur[ligne];
					
					lcd.print(vide);																					// efface la ligne
					lcd.setCursor(0, ligne);																			// reposition
					
					lcd.print(globale.afficheur[ligne]);
				}
			}
		}
	}
}

