
#ifndef parametre_configuration_h
#define parametre_configuration_h

// ************************ Parametres de configuration ************************

#ifndef TEMPSMANAGER
#define TEMPSMANAGER		20000																						// temps d'affichage du message badge manager (timer 0)
#endif

#ifndef TEMPSENLEVE
#define TEMPSENLEVE			5000																						// temps d'affichage du message enlevez votre badge (timer 1)
#endif

#ifndef TEMPSNOVICE
#define TEMPSNOVICE			10000																						// temps d'affichage du message non autorise (timer 2)
#endif

#ifndef TEMPSARRET
#define TEMPSARRET			5000																						// temps mini pour arreter la machine (timer 3)
#endif

#ifndef TEMPSATTENTE
#define TEMPSATTENTE		2000																						// temps d'attente avant l'affichage du message enlevez votre badge (timer 4)
#endif

#ifndef TEMPSPOSITION
#define TEMPSPOSITION		5000																						// temps d'affichage du message badge mal positionne (timer 5)
#endif

#ifndef TEMPSERREUR
#define TEMPSERREUR			5000																						// temps d'affichage du message erreur com serveur (timer 6)
#endif

#ifndef VITESSE
#define VITESSE				0x100																						// vitesse de cligno LED (multiple de 2)
#endif

#ifndef SCROLLING
#define SCROLLING			300																							// scrolling des texte trop long (attente en mS)
#endif

#ifndef RFIDEMPTY
#define RFIDEMPTY			4																							// nombre de lecture sans badge pour dire qu'il n'y a pas de badge
#endif

#ifndef RFIDREAD
#define RFIDREAD			2																							// nombre de lecture idententique pour la considerer juste
#endif

#ifndef RFIDCALL
#define RFIDCALL			50																							// appel lecture rfid toutes les 10ms
#endif

#ifndef RFIDUTIL
#define RFIDUTIL			10																							// longueur utile de l'id
#endif

#ifndef MESSAGE_ERREUR
#define MESSAGE_ERREUR		"Erreur interne voir le FabManager"															// message affiche en cas d'erreur de com avec le serveur
#endif

#ifndef MESSAGE_BADGE
#define MESSAGE_BADGE		"Presentez votre badge SVP"																	// message affiche pour demander le badge
#endif

#ifndef MESSAGE_ENLEVE
#define MESSAGE_ENLEVE		"Enlevez votre badge SVP"																	// message affiche pour enlever le badge
#endif

#ifndef MESSAGE_POSITION
#define MESSAGE_POSITION	"Badge mal positionne"																		// message badge mal positionne
#endif

#ifndef MESSAGE_ARRET
#define MESSAGE_ARRET		"Arret dans"																				// message d'arret
#endif

#ifndef MESSAGE_WIFI
#define MESSAGE_WIFI		"Attente WIFI"																				// message d'attente de connection wifi (setup() scrolling pas encore actif)
#endif

#ifndef MESSAGE_ACCUEIL
#define MESSAGE_ACCUEIL		"Attente serveur"																			// message d'accueil affiche sur lcd avant connection au wifi (setup() scrolling pas encore actif)
#endif

#endif		// parametre_configuration_h

