
#ifndef constante_materiel_h
#define constante_materiel_h

// *************************** Constantes materiels ****************************

/*
// -------------------- debut lecteur parallax --------------------

#ifndef RFIDSIZE
#define RFIDSIZE		11																								// lecteur RFID taille buffer reception (ID + null character)
#endif

#ifndef RFIDSTART
#define RFIDSTART		0x0A																							// lecteur RFID Start byte
#endif

#ifndef RFIDSTOP
#define RFIDSTOP		0x0D																							// lecteur RFID Stop byte
#endif

#ifndef RFIDBAUD
#define RFIDBAUD		2400																							// baud port serie
#endif

// -------------------- fin lecteur parallax --------------------
*/


// -------------------- debut lecteur RDM6300 --------------------

#ifndef RFIDSIZE
#define RFIDSIZE		13																								// lecteur RFID taille buffer reception (ID + null character)
#endif

#ifndef RFIDSTART
#define RFIDSTART		0x02																							// lecteur RFID Start byte
#endif

#ifndef RFIDSTOP
#define RFIDSTOP		0x03																							// lecteur RFID Stop byte
#endif

#ifndef RFIDBAUD
#define RFIDBAUD		9600																							// baud port serie
#endif

// -------------------- fin lecteur RDM6300 --------------------


#ifndef LCD_LIGNE
#define LCD_LIGNE		2																								// nombre de ligne du lcd
#endif

#ifndef LCD_COLONNE
#define LCD_COLONNE		16																								// nombre de colonne du lcd
#endif

#ifndef LCD_ADRESSE
#define LCD_ADRESSE		0x27																							// adresse interne du lcd (0x27 ou 0x3F en fonction du model)
#endif

#ifndef ARRET
#define ARRET			LOW																								// suivant le type de relais utilise mettre LOW/HIGH pour l'arret de la machine
#endif

#ifndef MARCHE
#define MARCHE			!ARRET																							// ne pas toucher inverse d'arret
#endif

#ifndef MACSIZE
#define MACSIZE			18																								// longueur adresse mac
#endif

#endif		// constante_materiel_h

