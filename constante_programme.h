
#ifndef constante_programme_h
#define constante_programme_h

// ************************** Constantes du programme **************************

#ifndef NOMBRE_TIMER
#define NOMBRE_TIMER 7
#endif

#ifndef TIMER_MANAGER
#define TIMER_MANAGER 0
#endif

#ifndef TIMER_NOVICE
#define TIMER_NOVICE 2
#endif

#ifndef TIMER_ARRET
#define TIMER_ARRET 3
#endif

#ifndef TIMER_ATTENTE
#define TIMER_ATTENTE 4
#endif

#ifndef TIMER_ERREUR
#define TIMER_ERREUR 6
#endif

#ifndef AVEC_COMPTE_REBOURS
#define AVEC_COMPTE_REBOURS 1
#endif

#ifndef SANS_COMPTE_REBOURS
#define SANS_COMPTE_REBOURS 0
#endif

#ifndef PAS_DE_TIMER
#define PAS_DE_TIMER 0
#endif

#ifndef WIFI
#define WIFI 1
#endif

#ifndef MILLISECONDE
#define MILLISECONDE 0
#endif

// pour globale.etat_machine

#ifndef MACHINE_ARRETEE
#define MACHINE_ARRETEE	0
#endif

#ifndef MACHINE_EN_MARCHE
#define MACHINE_EN_MARCHE 1
#endif

#ifndef MACHINE
#define MACHINE			1
#endif

#ifndef BUZZER
#define BUZZER			2
#endif

#ifndef ROUGE
#define ROUGE			4
#endif

#ifndef VERT
#define VERT			8
#endif

#ifndef BLEU
#define BLEU			16
#endif

#ifndef CLIGNO
#define CLIGNO			32
#endif

//couleur

#ifndef NOIR
#define NOIR	0
#endif

#ifndef ORANGE
#define ORANGE	ROUGE | VERT
#endif

#ifndef JAUNE
#define JAUNE	VERT | BLEU
#endif

#ifndef ROSE
#define ROSE	BLEU | ROUGE
#endif

#ifndef BLANC
#define BLANC	ROUGE | VERT | BLEU
#endif

// hierarchie des badges
// courant, pere, grand pere, arriere grand pere

#ifndef COURANT
#define COURANT 0
#endif

#ifndef PERE
#define PERE 1
#endif

#ifndef GRAND
#define GRAND 2
#endif

#ifndef ARRIERE
#define ARRIERE 3
#endif

#endif	// constante_programme_h

