
# Branchement du proto

* la boite avec le lecteur de badge

un cable entre les deux boites

    orange ==> +5v
    marron ==> +12v
    bleu   ==> 0v (du 5v et du 12v)
    vert   ==> commande du relais

un cable part vers la led de puissance

    orange ==> led rouge
    vert   ==> led verte
    bleu   ==> led bleue
    marron ==> +11 à 24v

un circuit d'experimentation (voir fichier circuit_nodemcu.odg)

    un connecteur pour la led de puissance (5broches red=orange green=vert blue=bleu nc +12=marron)
    un connecteur pour la led rgb (5broches red=rouge green=vert blue=bleu nc masse=noir)
    un connecteur pour l'antenne du lecteur rfid
    un connecteur pour piloter le relais (vert au milieu)
    un connecteur d'alim (0 0 . ++ ++) ++ de +7 a 12v
    un cable pour l'afficheur LCD (vert=scl bleu=sda rouge=5v noir=masse)

l'afficheur LCD

l'antenne du lecteur RFID

une led rgb

* la boite d'alimentation de la machine

un cable qui la relie avec la boite du lecteur de badge
un cable d'alim male a relier a une prise de courant
un cable d'alim femele pour l'alimentation de la machine
le contacteur de puissance
le petit relais de commande du contacteur
une alim 5v
une alim 12v


