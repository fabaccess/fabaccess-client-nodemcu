

# FabAccess client nodeMCU

## BUT:

	Chaque machine du fablab a son alimentation pilote par un module a l'aide d'un badge. L'utilisateur est informe a l'aide d'un voyant multicolore et d'un afficheur LCD.

## REALISATION:

	Le coeur du module est pilote par un nodeMCU (equivalent a un arduino avec wifi).
	La commande est effectuee par un relais (mecanique ou statique en fonction du type de machine a conmmander).
	L'affichage peut etre seconde par une sirene ou un afficheur geant.

## LES CHOIX:
	
* Utilisation du nodeMCU pour sa puissance, sa memoire et son wifi intege.

* Le relais mecanique pour les machines tres puissantes ou inductives.

* Le relais statique pour les autres machines.

* Lecteur de badge (RFID) pour la facilite d'utilisation. (interface homme / machine).

* LED RGB affichage de differentes couleurs en un seul voyant pour indiquer les droits de l'utilisateur sur la machine (non autorise, autorisation accompagne, autorise, erreur de connection).

* Afficheur LCD pour avoir des messages clairs.

## LES FICHIERS:

* a_faire.md

	les modifications du programmes
	les ammeliorations du programmes
	les possibilites d'evolution du programme
	
* readme.md

	ce fichier d'explication
	
* constante_materiel.h

	definition de constante en fonction du materiel utilise
	ex: en fonction du lecteur RFID
	
* constante_programme.h

	definition de constante utilise par le programme
	ex: definition de couleur pour la led rgb
	ex: CLIGNO | ROUGE -> rouge clignotant
		
* debug.h

	sur une machine de dev 
		avec le #define DEBUG TRUE a la compil genere des traces pour la console
	sur une machine de prod si on ne veux pas de trace sur la console
		mettre le #define en commentaire ou cree le fichier debug.h vide
	
* definition_broche.h

	l'affectation des broches en fonction du programme
		
* FabAccess.ino

	le code pour l'IDE ARDUINO

* LCDs_12_CharSet_01.jpg

	contenu version asiatique a gauche
	contenu version europeenne a droite
	du controleur LCD.
	ce contenu est en ROM (fixe).
	on voit qu'on peut creer 8 caracteres perso en ram (col de gauche)
	
* parametre_configuration.h

	definition de constante pour changer le parametrage du programme
	ex: affichage sur le lcd
	ex: temps pour arreter la machine
	
* secret.h

	contient les codes pour la connection au wifi
		#define SSID "...."
		#define PSSW "...."
	el url de l'api
		#define API "...."
	
* variable_globale.h

	toutes les variables globales du programme sont groupes dans une structure (globale)
	
## LE CODE:

* librairies utilisees

	LiquidCrystal_I2C 	version 1.1.2
	ArduinoJson 		version 5.13.5

* configuration du wifi et de l'URL de l'api dans secret.h

* les I/O

reste uniquement D0 et A0

* les fonctions

1. setup()

        * configuration et initialisation des broches, du lecteur RFID, du LCD
        * connection au wifi
        * envoi de l'adresse mac au serveur en reponse on a le nom du module 
            et le mode de gestion du badge (permanent ou ponctuel)
        * activation du lecteur RFID
        * affichage message "votre badge svp"

2. attente(par1, par2)

	fonction equivalente a delay() avec affichage d'un caractere tournant sur le lcd
	
	si 1er parametre = WIFI attente infini de connection au wifi
	sinon la duree de l'attente est dans le parametre 2 limitee a 64k mS

3. fonction requete_serveur(par1, par2, par3, par4)

        appel de l'api et traitement de la reponse

        * si par1 == 'i' .../init par2 adresse mac du module par3 et par4 null

            affichage nom du module sur la 1ere ligne du lcd
            memorisation de la securite de la machine
            	l'utilisateur doit rester a cote de la machine
            ou
            	l'utilisateur peut faire autre chose

        * si par1 == 'd' .../debut par2 adresse mac du module par3 identifiant du badge par4 null

        	affichage du message recu sur le lcd
        	affichage led en fonction des droit de l'utilisateur
        	commande du relais (alimentation du peripherique)

        * si par1 == 'f' .../fin par2 adresse mac du module par3 identifiant du badge par4 null

        	affichage du message recu sur le lcd
        	eteint la led
        	commande du relais (arret du peripherique)

        * si par1 == 'a' .../aide par2 adresse mac du module par3 badge avec aide par4 badge manager

        	affichage du message recu sur le lcd
        	eclaire la led en vert
        	commande du relais (alimentation du peripherique)

        * si par1 == 'v' .../veriffin par2 adresse mac du module par3 identifiant du badge par4 null

        	si la machine est en marche et qu'il y a lecture de badge
        	on verifie si son proprietaire a les droits d'utiliser la machine

4. fonction couleur()

	La fonction couleur() permet suite a couleur(parametre) de reafficher la bonne couleur
        	
5. fonction couleur(parametre)

	La fonction couleur permet d'affecter une couleur a la led sans la memoriser
	- indispensable dans le setup (puisque loop() pas encore actif)
	- utile pour l'indication d'acces au serveur
	
        * si parametre NOIR eteint la led rgb
		* si parametre ROUGE VERT BLEU  eclaire la led en rouge vert ou bleu
			possibilite de mettre un | pour composer les couleurs ex: BLEU | ROUGE
        * si parametre CLIGNO | couleur fait clignoter la couleur
	
6. fonction affichage(par1, par2)
		
		* par1 String texte a afficher sur le lcd
		* par2 no de ligne a afficher
			avec scrolling si longueur texte > 15 (voir parametre_configuration)
			gestion des accents
			gestion changement de texte
			suppression des caracteres a droite si besoin

7. fonction lecture_badge()

        lecture du badge non bloquante
        	
        	* recherche caractere de debut et de fin de transmission
        	
        	* plusieur lecture et comparaison si egale -> traite
        		(voir constante_materiel)

        	* si module en mode badge fixe
    		    si machine eteinte -> demarre
    		    si on enleve le badge au moins 5s -> arrete
        	
        	* si module en mode badge mobile
    		    si machine eteinte -> demarre et si on laisse le badge trop longtemps bip + message
    		    si machine en marche -> si laisse le badge au moins 5s -> eteint

8. fonction loop()

		* commande du relais de mise en marche
        * pilotage du buzzer (si passif)
        * pilotage de la led (et cligno si necessaire)
        * controle la connection wifi (led bleu et attente reconnection)
        * appel a la fonction lecture_badge toutes les 50ms (voir parametre_configuration.h)
        * gestion des timers (changement affichage plus indication led et buzzer)
        * gestion de l'affichage du lcd (scrolling des textes trop long)

